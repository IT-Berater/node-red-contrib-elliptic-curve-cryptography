module.exports = function (RED) {
  const NodeName = 'bip39 wordlist'

  function BIP39WordlistNode (config) {
    RED.nodes.createNode(this, config)
    let bip39
    try {
      bip39 = require('bip39')
    } catch (err) {
      console.log('Crypto bip39 support is disabled! ' + err)
    }

    // wenn keine Worte übergeben werden werden welche generiert
    // wenn Worte übergeben werden, werden sie auf Gültigkeit überprüft, wenn gültig zurückgeliefert bei Fehler wird "ERROR: Ungültige Worte: WORTE" geliefert
    function bip39Wordlist (worte) {
      if (worte === '') {
        const mnemonic = bip39.generateMnemonic()
        return mnemonic
      }

      if (bip39.validateMnemonic(worte)) {
        return worte
      } else {
        return 'ERROR: Ungültige Worte: ' + worte
      }
    }

    function getHexBip39 (worte) {
      return bip39.mnemonicToSeedSync(worte).toString('hex')
    }

    function getEntropy (worte) {
      return bip39.mnemonicToEntropy(worte)
    }

    const node = this
    this.on('input', function (msg) {
      msg.payload = bip39Wordlist(msg.payload)
      msg.seed = getHexBip39(msg.payload)
      msg.entropy = getEntropy(msg.payload)
      node.send(msg)
    })
  }
  RED.nodes.registerType(NodeName, BIP39WordlistNode)
}
