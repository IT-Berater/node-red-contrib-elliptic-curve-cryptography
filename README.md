# node-red-contrib-elliptic-curve-cryptography

[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

I need a Node in NodeRed that generate similar result what this command generate in linux. xxd  creates  a hex dump of a given file or standard input.  It can also convert a hex dump back to its original binary form. Reverse (-r) operation: convert (or patch) hexdump into binary. Also (-p) known as plain hexdump style.

On the commandline:

`echo -n '16432a207785ec5c4e5a226e3bde819d' | xxd -r -p | sha256sum -b | awk '{print $1}'`

This command outputs:

`7aa0c5dba5ab65301e23532af8a1730d40068cd32ebafaa5765de0d9bd9ef60f`


The node "hash-sha256-hex" has dependencies with "[eosjs-ecc](https://www.npmjs.com/package/eosjs-ecc/v/4.0.7)" "4.0.7" from https://github.com/IT-Berater/eosjs-ecc for the hash sha256 (binary) function.

What this command does is first given an hexadecimal string, convert it to binary, and display the sha256 hash of the binary data in hexadecimal.

Different libraries put simply the output with sha256 hash the string.

If you need the normal sha256 with different hash output, you can use the [node-red-contrib-cryptography](https://gitlab.com/IT-Berater/node-red-contrib-cryptography) Node:

`echo -n '16432a207785ec5c4e5a226e3bde819d' | ./sha256sum`

This command other outputs:

`dc88aa37112eec5b02c3b082cb581ee567a9de1c7d4a50851bd3c6e6707c96cc`

## BIP39 Wordlist

The node "bip39 wordlist" used the dependencies "bip39": "3.0.3". You can simple generate a BIP39 Wordlist with 12 words. Its gnerate a hex Seed for this words.

You can typed your onwn words and create the Checksum and Seed. The node validates this input words.


Sample Flow:
![image](http://blog.wenzlaff.de/wp-content/uploads/2021/02/wenzlaff.de-2021-02-28-um-19.54.46.png)

Gui:

![image](http://blog.wenzlaff.de/wp-content/uploads/2021/02/wenzlaff.de-2021-02-28-um-19.54.31.png)





