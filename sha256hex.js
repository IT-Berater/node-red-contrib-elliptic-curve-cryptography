module.exports = function (RED) {
  const NodeName = 'hash sha256 hex'

  function HashSha256HexNode (config) {
    RED.nodes.createNode(this, config)
    let ecc
    try {
      ecc = require('eosjs-ecc')
    } catch (err) {
      console.log('Crypto eosjs-ecc support is disabled! ' + err)
    }

    function sha256 (hexstr) {
      const buffer = new Uint8Array(hexstr.match(/[\da-f]{2}/gi).map(function (h) {
        return parseInt(h, 16)
      }))
      return ecc.sha256(buffer)
    }

    const node = this
    this.on('input', function (msg) {
      msg.payload = sha256(msg.payload)
      node.send(msg)
    })
  }
  RED.nodes.registerType(NodeName, HashSha256HexNode)
}
